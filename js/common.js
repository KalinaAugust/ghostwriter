$(document).ready(function () {

    (function (f) {
        "use strict";
        "function" === typeof define && define.amd ? define(["jquery"], f) : "undefined" !== typeof module && module.exports ? module.exports = f(require("jquery")) : f(jQuery)
    })(function ($) {
        "use strict";
        function n(a) {
            return !a.nodeName || -1 !== $.inArray(a.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"])
        }

        function h(a) {
            return $.isFunction(a) || $.isPlainObject(a) ? a : {top: a, left: a}
        }

        var p = $.scrollTo = function (a, d, b) {
            return $(window).scrollTo(a, d, b)
        };
        p.defaults = {axis: "xy", duration: 0, limit: !0};
        $.fn.scrollTo = function (a, d, b) {
            "object" === typeof d && (b = d, d = 0);
            "function" === typeof b && (b = {onAfter: b});
            "max" === a && (a = 9E9);
            b = $.extend({}, p.defaults, b);
            d = d || b.duration;
            var u = b.queue && 1 < b.axis.length;
            u && (d /= 2);
            b.offset = h(b.offset);
            b.over = h(b.over);
            return this.each(function () {
                function k(a) {
                    var k = $.extend({}, b, {
                        queue: !0, duration: d, complete: a && function () {
                            a.call(q, e, b)
                        }
                    });
                    r.animate(f, k)
                }

                if (null !== a) {
                    var l = n(this), q = l ? this.contentWindow || window : this, r = $(q), e = a, f = {}, t;
                    switch (typeof e) {
                        case "number":
                        case "string":
                            if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)) {
                                e = h(e);
                                break
                            }
                            e = l ? $(e) : $(e, q);
                        case "object":
                            if (e.length === 0)return;
                            if (e.is || e.style) t = (e = $(e)).offset()
                    }
                    var v = $.isFunction(b.offset) && b.offset(q, e) || b.offset;
                    $.each(b.axis.split(""), function (a, c) {
                        var d = "x" === c ? "Left" : "Top", m = d.toLowerCase(), g = "scroll" + d, h = r[g](),
                            n = p.max(q, c);
                        t ? (f[g] = t[m] + (l ? 0 : h - r.offset()[m]), b.margin && (f[g] -= parseInt(e.css("margin" + d), 10) || 0, f[g] -= parseInt(e.css("border" + d + "Width"), 10) || 0), f[g] += v[m] || 0, b.over[m] && (f[g] += e["x" === c ? "width" : "height"]() * b.over[m])) : (d = e[m], f[g] = d.slice && "%" === d.slice(-1) ? parseFloat(d) / 100 * n : d);
                        b.limit && /^\d+$/.test(f[g]) && (f[g] = 0 >= f[g] ? 0 : Math.min(f[g], n));
                        !a && 1 < b.axis.length && (h === f[g] ? f = {} : u && (k(b.onAfterFirst), f = {}))
                    });
                    k(b.onAfter)
                }
            })
        };
        p.max = function (a, d) {
            var b = "x" === d ? "Width" : "Height", h = "scroll" + b;
            if (!n(a))return a[h] - $(a)[b.toLowerCase()]();
            var b = "client" + b, k = a.ownerDocument || a.document, l = k.documentElement, k = k.body;
            return Math.max(l[h], k[h]) - Math.min(l[b], k[b])
        };
        $.Tween.propHooks.scrollLeft = $.Tween.propHooks.scrollTop = {
            get: function (a) {
                return $(a.elem)[a.prop]()
            }, set: function (a) {
                var d = this.get(a);
                if (a.options.interrupt && a._last && a._last !== d)return $(a.elem).stop();
                var b = Math.round(a.now);
                d !== b && ($(a.elem)[a.prop](b), a._last = this.get(a))
            }
        };
        return p
    });
    //scrollTo lib

    $(".to-top").click(function () {
        $.scrollTo($("#main-form"), 800, {
            offset: -90
        });
    });

    $("#logo-to-up").click(function () {
        $.scrollTo($("html"), 800, {
            offset: 0
        });
    });

    var menu = $('.header__menu');
    var header = $('header.header');

    $("body").click(function (event) {
        var target = $(event.target);

        if (target.is('.header__burger') || target.is('.nav-span')) {
            $('#nav-icon1').toggleClass('open');
            menu.toggleClass('menu-open');
        } else if (target.closest(header).hasClass('header')) {
            return;
        } else {
            menu.removeClass('menu-open');
            $('#nav-icon1').removeClass('open');
        }
    });


    $('.button__pop-up').magnificPopup({
        fixedContentPos: true
    }); //pop-up


    $.fn.datepicker.language['de'] = {
        days: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        daysShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
        daysMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        months: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthsShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        today: 'Heute',
        clear: 'Aufräumen',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    };

    $('.date').datepicker({
        language: 'de'
    });


    $('.second-section__selectize').selectize({
        create: true,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    });


    $("#pop-up__form").submit(function () {
        $.ajax({
            type: "GET",
            url: "mail-pop-up.php",
            data: $("#pop-up__form").serialize()
        }).done(function () {
            $.magnificPopup.open({
                items: {
                    src: '<div id="pop-up-success" class="pop-up"><div class="pop-up__heading pop-up__heading-success">Vielen Dank! Wer werden Sie in Kürze anrufen.</div></div>',
                    type: 'inline'
                }
            });
        });
        return false;
    });

    $(".pop-up-author__form").submit(function () {
        $.ajax({
            type: "GET",
            url: "mail-pop-up-author.php",
            data: $(".pop-up-author__form").serialize()
        }).done(function () {
            $.magnificPopup.open({
                items: {
                    src: '<div id="pop-up-success" class="pop-up"><div class="pop-up__heading pop-up__heading-success">Bewirb dich jetzt!<br>Wir werden uns in Kürze mit Ihnen in Verbindung setzen!</div></div>',
                    type: 'inline'
                }
            });
        });
        return false;
    });


    // ----main form---- //


    var destination = "http://client.ghost-writerservice.de";

    function generatePassword() {
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    $("#main-form").submit(function (event) {

        event.preventDefault();

        wortypesObj = {
            10: 'Hausarbeit',
            63: 'Masterarbeit',
            2: 'Bachelorarbeit'
        };

        $.ajax({
            type: "GET",
            url: "mail.php",
            data: $("#main-form").serialize() + "&worktype=" + wortypesObj[$('[name = type]').val()]
        });



        var worktype = $('[name = type]').val(),
            subjectText = $('[name = subjectText]').val(),
            pages = $('[name = pages]').val(),
            date = $('[name = date]').val(),
            theme = $('[name = theme]').val(),
            name = $('[name = name]').val(),
            phone = $('[name = phone]').val(),
            email = $('[name = email]').val(),

            data_order = {
                "worktype": worktype,
                "subjectText": subjectText,
                "pagesAll": pages,
                "deadLine": date,
                "description": theme
            },

            data_client = {
                "a_aid": "a1985216",
                "fio": name,
                "phone": phone,
                "email": email,
                "password": generatePassword()
            };


        var client = $.post(destination + "/rest/client/", data_client, function (data, textStatus, jqXhr) {
            return data;
        });

        client.done(function(data, textStatus, jqXhr) {

        var token = data.Token;

        document.cookie = "token=" + token + "; path=/; domain=." + 'ghost-writerservice.de' + ";";


        var order = $.ajax({
            headers: {
                'token': token,
                'language': 'de'
            },
            url: destination + "/rest/client/orders/",
            type: "POST",
            data: JSON.stringify(data_order),
            contentType: "application/json"
        });


        $.magnificPopup.open({
            items: {
                src: '<div class="pop-up"><div class="pop-up__heading pop-up__heading-success">Vielen Dank. Wir haben Ihre Anfrage erhalten. Sie werden nun automatisch zu Ihrem Account weitergeleitet.</div></div>',
                type: 'inline'
            }
        });

        window.setTimeout(function () {
            var url = "http://client.ghost-writerservice.de";
            $(location).attr('href', url);
        }, 5000);



        });


        client.fail(function(jqXHR, textStatus) {
            $.magnificPopup.open({
                items: {
                    src: '<div class="pop-up"><div class="pop-up__heading pop-up__heading-success">Bitte loggen Sie sich zum Ausfüllen des Formulars ein.</div></div>',
                    type: 'inline'
                }
            });
        });



    });


});


